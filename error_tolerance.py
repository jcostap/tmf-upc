#Python imports
import os, argparse, sys
from subprocess import check_output, run
import json
import shutil

#Custom imports
from maps_parse import parse_mem_maps
from inject_mce_process import translation
from tui import check_input


PIDS_FILE = sys.path[0] + "/config/watch.pids"
BASE_BACKUP_DIR = "/etc/criu-mce-backup/"
CACHE_FILE = sys.path[0] + "/config/cache"

#The dictionary where we load the pa cache
PA_CACHE = {}


def is_pa_on_pid(pid, pa):
    """
        Parse the maps of the process with pid and translate all
        of the va's to match the pa. Uses a C extension to speed up
        the translation and add parallelism.
    """
    mem_reg = parse_mem_maps(pid)
    pid = int(pid)
    pa = int(pa, 16)
    for reg in reversed(mem_reg):
        try:
            output = run(["./is_my_pa.out", str(pid), str(reg.vstart), str(reg.vend), str(pa)]).returncode
            if(output == 0):
                return True
        except:
            pass

    return False


def read_pids():
    """
        Read the pids of interest from the PIDS_FILE.
    """
    pids = []
    with open(PIDS_FILE, 'r') as pids_file:
        for line in pids_file.readlines():
            pids.append(int(line))
    return pids


def read_cache_from_pers():
    global PA_CACHE

    if(not os.path.isfile(CACHE_FILE)):
        return

    with open(CACHE_FILE,'r') as cache_file:
        PA_CACHE = json.loads(cache_file.read())


def write_cache_to_pers():
     with open(CACHE_FILE,'w') as cache_file:
        cache_file.write(json.dumps(PA_CACHE))


def is_in_cache(pa):
    if(pa in PA_CACHE):
        return PA_CACHE[pa]
    return 0


def update_cache(pa, pid):
    global PA_CACHE
    PA_CACHE[pa] = pid


def check_pids(pa):
    """
        Check if the pa belongs to any of the pids
        of interest.
    """
    pids = read_pids()

    #if the PA is in the cache remove the PID from the list and put it first.
    cache_pid = is_in_cache(pa)
    if(cache_pid != 0):
        if(cache_pid in pids):
            pids.remove(cache_pid)
        pids.insert(0, cache_pid)

    for pid in pids:
        if(not os.path.isfile("/proc/"+str(pid)+"/pagemap")):
            continue
        anw = is_pa_on_pid(pid, pa);
        if( anw == True):
            update_cache(pa, pid)
            return pid
    return 0


def get_last_bk_version(pr_dir):
    version = 0

    #The directories should not be modified externally
    filenames = os.listdir(pr_dir) # get all files and directories
    for filename in filenames: # loop through all the files and folders
        #Find how many dumps you already have
        version = version + 1

    return version


def save_incr_proc(pid):
    """
        Save the process state incrementally by saving memory pages
        only. This is faster than full save but a full save is
        required to perform th restore.
    """
    #create the destination directory
    base_directory = BASE_BACKUP_DIR + "pr" + str(pid)
    os.makedirs(base_directory, exist_ok=True)
    version = get_last_bk_version(base_directory)

    directory = base_directory + "/"  + str(version)

    if(version > 0):
        prev_dir =  "../" + str(version - 1)

    os.makedirs(directory, exist_ok=True)

    #try to dump the process tree
    try:
        if(version > 0):
            retval = run(["criu", "dump", "-t", str(pid), "-j", "-D", directory,
                        "--leave-running", "--track-mem", "--prev-images-dir", prev_dir]).returncode
        else:
            retval = run(["criu", "dump", "-t", str(pid), "-j", "-D", directory,
                        "--leave-running", "--track-mem"]).returncode
    except:
        print("Pre-dumping was attemped.")

    print("Predump")


def save_proc(pid):
    """
        Checkpoint the full process state.
    """
    #create the destination directory
    directory = BASE_BACKUP_DIR + "p-" + str(pid)

    os.makedirs(directory, exist_ok=True)

    #check if there is a predump for this process.
    pr_dir =  BASE_BACKUP_DIR + "pr" + str(pid)
    if(not os.path.exists(pr_dir)):
       #try to dump the process tree
        try:
            retval = run(["criu", "dump", "-t", str(pid), "-j", "-D", directory]).returncode
        except:
            print("Dumping was attemped but maybe failed..")
    else:
        pr_rel_dir = "../pr" + str(pid) + "/" + str(get_last_bk_version(pr_dir)-1)
        #try to dump the process tree by using predump files
        try:
            retval = run(["criu", "dump", "-t", str(pid), "-j",
                        "-D", directory, "--prev-images-dir", pr_rel_dir, "--track-mem"]).returncode

            #Clean pre-dump dir after dump
            #shutil.rmtree(pr_dir, ignore_errors=True)

            print("Using pre dump files in %s" % pr_rel_dir)
        except:
            print("Dumping was attemped. Pre-dump was used. Maybe it failed.")

    print("Dump")


def restore_proc(pid=None):
    """
        Check what processes were saved and provide the user
        with options to restore any of them.
    """

    if(not os.path.exists(BASE_BACKUP_DIR)):
        print("The %s does not exist" % BASE_BACKUP_DIR)
        exit(0)

    filenames = os.listdir(BASE_BACKUP_DIR) # get all files and directories
    pids = []
    for filename in filenames: # loop through all the files and folders
        if(os.path.isdir(BASE_BACKUP_DIR + filename)):
            if(filename[:2] == "p-"):
                pids.append((int(filename[2:]), 'Full'))

    for filename in filenames: # look for incremental backups
        if(os.path.isdir(BASE_BACKUP_DIR + filename)):
            if(filename[:2] == "pr"):
                if(not (int(filename[2:]), 'Full') in pids):
                    pids.append((int(filename[2:]), 'Incremental'))

    if(not pid):
        sel_pid = check_input("Select a pid to restore from %s:" % pids, type_=int,
                            range_=[p[0] for p in pids])
    else:
        sel_pid = pid

    directory = BASE_BACKUP_DIR + "p-" + str(sel_pid)
    if(not os.path.isdir(directory)):
        directory = BASE_BACKUP_DIR + "pr" + str(sel_pid)

    if(not os.path.isdir(directory)):
        print("Something went wrong with the backup dir.")
        exit(0)

    #try to restore the selected process
    try:
        retval = run(["criu", "restore",  "-j", "-D", directory, "--restore-detached"]).returncode
    except:
        print("Restore was attempted but maybe has failed..")

    print("Restore")


def error_analysis(pa, count, timeframe):
    """
        Analyse the error frequency and take appropriate action.
    """
    THRES_LOW = 2
    THRES_MID = 6
    THRES_HIGH = 10

    pid = check_pids(pa)

    #The error occured on process of no interest
    if(pid == 0):
        return

    if(count >= THRES_LOW and count < THRES_HIGH ):
        #Not too many errors occured on important process
        save_incr_proc(pid)

    if(count >= THRES_HIGH):
        #Too many errors occured on important process
        save_proc(pid)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(epilog = """Checks if the pa belongs to any pid defined in %s.
                                                    Each pid should take exactly one line.""" % PIDS_FILE)

    parser.add_argument("-p", "--pa", required=False, help="The PA to check if it belongs to any PID.")
    parser.add_argument("-m", "--msg", required=False, help="Env. variable that comes from mcelog.")
    parser.add_argument("-t", "--threshold", required=False, help="Env. variable that comes from mcelog.")
    parser.add_argument("-r", "--restore", required=False, action='store_true', help="Start process restore.")
    parser.add_argument("-d", "--dir", required=False, help="The directory to store the  backup.")
    parser.add_argument("--pid", required=False)

    args = parser.parse_args()

    if(os.getuid() != 0):
        print("I need root privileges to run!")
        sys.exit(0);

    #Read the PA cache from persistency
    read_cache_from_pers()

    if(args.dir):
        BASE_BACKUP_DIR = args.dir

    if( args.msg == None and args.pa):
        pid = check_pids(arg.pa)
        if(pid != 0):
            print("PA belongs to the proces %s" %  pid)
        else:
            print("PA doesen't belong to any pid that I know.")
    elif(args.msg and args.threshold):
        pa = args.msg.split()[5]
        count = int(args.threshold.split("in")[0])
        timeframe = int(args.threshold.split("in")[1].split('h')[0])
        error_analysis(pa, count, timeframe)

    if(args.restore):
        #Check available processes to restore and ask
        if(args.pid):
            restore_proc(pid=args.pid)
        else:
            restore_proc()

    #Write the cache to persistency
    write_cache_to_pers()
