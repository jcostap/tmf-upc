# MCE injection and tolerance at OS level.

This work started as a part of a master thesis which
can be found [here](https://drive.google.com/file/d/11H5yAkjuM6ybI0MbEtECplMO1EFaPOYq/view?usp=sharing). In this document you find
all the necessary details in order to
understand and use the above tools.

## Dependencies
You have to compile and install:

1. mcelog
2. mce-inject (reminder: load the **mce-inject** module to linux kernel using the modrpobe command)
3. criu

For mcelog and mce-inject find more [here](https://github.com/andikleen/mce-test/blob/master/doc/howto.txt). Note that mcelog must be started in daemon mode.

For criu find more [here](https://criu.org/Main_Page).

## VA and PID to PA (va_pid_to_pa.c)
This tool translates the virtual address of a particular process to a physical address. It also has
self validation options to verify it's correctness.  Check **./va_pid_to_pa.out -h** for more options.


## Dummy process (dummy_proc.c)
A process that can be used to verify the previous tool. This process will print a VA and a value of
it's variable. You can try to change this value using the previous tool which will write to **/dev/mem**
using the physical address of the variable. However, in order to
access **/dev/mem** you have to recompile the kernel with  **CONFIG_STRICT_DEVMEM = n** configuration option.


## Inject MCE to process (inject_mce_to_process.py)
This tool will allow you to pass any error description file written for mce-inject. The tool
will replace any appearance of **0x&** with a physical address obtained from the VA and the pid
that will be provided through options. This tool is helpful for people already familiar with
mce-inject input files, who want to introduce MCEs to a specific process.  


## Tui (tui.py)
A more user friendly to tool which will guide you through the process of basic MCE injection
to a specific process. This tool will help you to insert random errors to the process physical
address space or choose specific regions from the process mapping. This tool does not support
all the possible options of mce-inject, the previous tool can be used by  advanced users.

You can extend this tool to any error template with one step:

1. Add the template to **err_templates/** directory with name: new_error.err.


## Error Tolerance (error_tolerance.py)
This tool can be used for two reasons. First a user can provide the tool with a physical address
and the tool will return if this PA is actually used by any process in the list. The list is located
in the **config/watch.pids**, the user must add any PID, he is interested in, to this list before running the tool.

The second reason is to connect this tool to the mcelog triggers for page errors. To do that 3 steps are involved.

1. Configure the page error triggers in **/etc/mcelog/mcelog.conf** 
2. Add the following lines of code to **/etc/mcelog/page-error-trigger.local**

```console
SCRIPT="error_tolerance.py"
cd $( dirname $(realpath $SCRIPT))
sudo python3 $SCRIPT --msg "$MESSAGE" -t "$THRESHOLD" > config/log.out  2>&1
cd /etc/mcelog
```
3. Enter the directory with the script and symlink the **error\_tolerance.py** to the **/etc/mcelog/**

```console
sudo ln -s  $(readlink -f error_tolerance.py)  /etc/mcelog/error_tolerance.py
```

You can find both the error log and the output log from this tool in the **config/log.out** file.
When a certain amount of errors happens on a memory page the script will identify the process that
is affected (one of the processes in **config/watch.pids**) and will try to back it up in a predefined directory (by default **/etc/criu-mce-backup**). You can modify this directory with the **-d** switch in the second step above. To restore any process use the **-r** option which will provide you with the available PIDs to restore.

## Compilation
The .**c** files can be compiled by executing **make** in the current directory. This includes the
first two tools. We compiled those using the **GCC 7.2 compiler**. The other three are written in **Python 3** and were
tested using the **Python 3.6.3** interpreter.


## Execution
Run any tool with -h option to see use instructions. All of the tools except the dummy process
must be executed with root privileges.
