#include "reusable.h"

/*
 * Ensures root privileges and
 * correct pid.
 * */
void check_root_pid(pid_t pid)
{
    if(geteuid() != 0)
    {
        fprintf(stderr, "Run as root!\n");
	fprintf(stderr, "We need privileges to access /proc/pid/pagemap and/or /dev/mem.\n");
	exit(1);
    }

    // The options below need the pid, it can't be 0
    if(pid == 0)
    {
        fprintf(stderr, "Wrong/No pid given. Can't go on.\n");
	exit(1);
    }

}


/*
 *  Translates the virtual address
 *  to physical.
 *
 *  @param addr: virtual address
 * */
unsigned long long
va_to_pa(void* addr, pid_t pid)
{
    unsigned long long  page_frame_number;
    unsigned int offset;
    unsigned long long pa;
    int page_size = getpagesize();

#ifdef DEBUG
    printf("Page size:0x%x\n", page_size);
#endif

    // Get the voffset == poffset
    offset = (unsigned long)addr % page_size;

    // Get the page frame the buffer is on
    page_frame_number = get_page_frame_number_of_address(addr, pid);

    if(page_frame_number == 0)
        return 0;

#ifdef DEBUG
    printf("Page frame: 0x%x\n", page_frame_number);
    printf("Offset: 0x%x\n", offset);
#endif

    // Turn page frame number into page base addr and add the offset
    pa = ((unsigned long long)page_frame_number * page_size) + offset;

    return pa;
}


/*
 * Returns the page-frame number of a
 * virtual address by reading an offset
 * in the pagemap file.
 *
 * @param addr: Virtual address.
 *
 * */
unsigned long long
get_page_frame_number_of_address(void* addr, pid_t pid)
{
    // Open the pagemap file for the current process
    FILE* pagemap;

    // Build the pagemap path
    char file[128];
    char tmp[64];
    tmp[0] = '\0';
    file[0] = '\0';

    strcat(file,"/proc/");

    if(pid == -1)
	strcat(tmp, "self");
    else
	sprintf(tmp, "%d", pid);

    strcat(file, tmp);
    strcat(file, "/pagemap");

#ifdef DEBUG
    printf("Pagemap file: %s\n",file);
#endif

    if(!(pagemap = fopen(file,"rb")))
    {
	fprintf(stderr, "Can't open pagemap file. Check the pid.\n");
	exit(1);
    }

    // Seek to the page that the buffer is on
    unsigned long long offset = (unsigned long long)addr / getpagesize() * PAGEMAP_LENGTH;
    if (fseek(pagemap, (unsigned long long)offset, SEEK_SET) != 0)
    {
        fprintf(stderr, "Failed to seek pagemap to proper location.\n");
        exit(1);
    }

    // The page frame number is in bits 0-54 so read the first 7 bytes and clear
    // the 55th bit
    unsigned long long page_frame_number = 0;
    fread(&page_frame_number, 1, PAGEMAP_LENGTH - 1, pagemap);

    //Mask the first 55 bits
    page_frame_number &= 0x7FFFFFFFFFFFFF;

    fclose(pagemap);

    return page_frame_number;
}
