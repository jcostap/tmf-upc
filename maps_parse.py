import re, sys, os

class MemRegion:
    """
        Attributes and methods related to mem maps
        parsing.
    """
    pid = 0
    vstart = 0
    vend = 0
    pread = 0
    pwrite = 0
    pexec = 0
    pshare = 0
    file_offset = 0
    major_dev_num = 0
    minor_dev_num = 0
    inode = 0
    filepath = ''
    region_tag = ''

    def __init__(self, pid):
        self.pid = pid

    def parse_region(self, line):
        mr = re.match(r'([0-9A-Fa-f]+)-([0-9A-Fa-f]+) ([-r])', line)
        self.vstart = int(mr.group(1), 16)
        self.vend = int(mr.group(2), 16)

    def range_(self):
        return range(self.vstart, self.vend)

def parse_mem_maps(pid):
    """
        Parses Mem Maps for a given process and
        creates a list with the mappings of different
        regions.
    """
    mem_regions = []
    maps_file_name = "/proc/" + str(pid) + "/maps"
    with open(maps_file_name, 'r') as maps_file:
        for line in maps_file.readlines():
            mem_reg = MemRegion(pid)
            mem_reg.parse_region(line)
            mem_regions.append(mem_reg)
    return mem_regions
